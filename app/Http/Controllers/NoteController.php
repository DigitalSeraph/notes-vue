<?php

namespace App\Http\Controllers;

use App\Note;
use Auth;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Note::where(['user_id' => Auth::user()->id])
                        ->with('user:id,name,email')
                        ->get();

        return response()->json([
            'notes' => $notes,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'body'  => 'required',
        ]);

        $note = Note::create([
            'title'     => request('title'),
            'body'      => request('body'),
            'user_id'   => Auth::user()->id
        ]);

        return response()->json([
            'note'    => $note->load('user'),
            'message' => 'Success'
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Note $note)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'body'  => 'required',
        ]);
 
        $note->title = request('title');
        $note->body = request('body');
        $note->save();
 
        return response()->json([
            'message' => 'Note updated successfully!'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Note  $note
     * @return \Illuminate\Http\Response
     */
    public function destroy(Note $note)
    {        
        try {
            $note->delete();
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Not able to delete note!'
            ], 409);
        }

        return response()->json([
            'message' => 'Note deleted successfully!'
        ], 200);

    }

    /**
     * Show the notes index page
     *
     * @return \Illuminate\Http\Response
     */
    public function indexPage()
    {
        return view('pages.notes');
    }
}
