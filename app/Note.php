<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Note extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'body',
        'user_id',
    ];

    /**
     * Get the user that owns this note
     */
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    /**
     * Format the created_at field as a Carbon object (DateTime)
     *
     * @param  string $format    DateTime format to return or shortcut term
     *
     * @return string            DateTime formatted as a string
     */
    public function createdAt($format = 'default') {
        /** @var \Carbon\Carbon */
        $dateTime = Carbon::parse($this->created_at);

        $this->formatDateTime($dateTime, $format);

        return $dateTime;
    }

    /**
     * Format the modified_at field as a Carbon object (DateTime)
     *
     * @param  string $format    DateTime format to return or shortcut term
     *
     * @return string            DateTime formatted as a string
     */
    public function modifiedAt($format = 'default') {
        /** @var \Carbon\Carbon */
        $dateTime = Carbon::parse($this->modified_at);

        $this->formatDateTime($dateTime, $format);

        return $dateTime;
    }

    /**
     * Format a Carbon (datetime) object
     *
     * @param  \Carbon\Carbon &$dateTime A Carbon (DateTime) object
     * @param  string $format    DateTime format OR shortcut term
     *
     * @return string            Formatted Carbon (DateTime) string
     */
    public function formatDateTime(Carbon &$dateTime, $format = 'default') {
        switch ($format) {
            case 'short':
                $dateTime = $dateTime->format('D g:i a');
                break;
            
            case 'long':
                $dateTime = $dateTime->toDayDateTimeString();
                break;
            
            case 'diffHuman':
                $dateTime = $dateTime->diffForHumans();
                break;

            case 'default':
                $dateTime = $dateTime->toDateTimeString();
                break;
            
            default:
                $dateTime = $dateTime->format($format);
                break;
        }

        return $dateTime;
    }
}
