<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App;
use PDO;
use DB;
use PDOException;

class DatabaseCreateCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'notes:db:create';


    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notes:db:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command creates a new database for the application';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $database = env('DB_DATABASE', false);
        $port = env('DB_PORT');
        $username = env('DB_USERNAME');
        $password = env('DB_PASSWORD');

        if (! $database) {
            $this->info('Skipping creation of database as env(DB_DATABASE) is empty');
            return;
        }

        try {
            $pdo = $this->getPDOConnection(env('DB_HOST'), $port, $username, $password);

            $pdo->exec("CREATE DATABASE IF NOT EXISTS ${database}");

            $this->info(sprintf('Successfully created %s database', $database));
        } catch (PDOException $exception) {
            $this->error(sprintf('Failed to create %s database, %s', $database, $exception->getMessage()));
        }
    }

    /**
     * @param  string $host
     * @param  integer $port
     * @param  string $username
     * @param  string $password
     * @return PDO
     */
    private function getPDOConnection($host, $port, $username, $password)
    {
        return new PDO("mysql:host=${host};port=${port};charset=utf8mb4", $username, $password);
    }
}
