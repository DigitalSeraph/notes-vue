# Notes Challenge

Coded by David Miller. Available on BitBucket.

[TOC]

## About

This is a challenge coding project for [SharpSpring](https://sharpspring.com/). 

> To demonstrate your coding competency, you'll be creating a very small note
management system. There are some opportunities for creativity in the project
outlined below, so please use this project to put your best face forward and
show us the quality of work that you'd contribute if you were working on our
product.

## Requirements

You will either need a working [homestead](https://laravel.com/docs/5.5/homestead) setup, or you will need to meet the following requirements if running a server locally:

- php7.1
- composer
- php-mbstring
- php-xml
- php-curl
- php-zip
- php-mysql
- php-sqlite3
- npm or yarn
- mysql (or mariadb)

## Setup

1. Clone this repostiory
    - `git clone git@bitbucket.org:DigitalSeraph/notes-vue.git`
2. Copy and modify the dotenv file for your local environment: `cp .env.example .env`
    - Change the following variables:
        - `DB_DATABASE` - the name of the local database to use
        - `DB_USERNAME` - the database user
        - `DB_PASSWORD` - the database user password
        - `APP_URL` - the url to run the site at
    - Optional:
        - `DEV_USER_NAME`, `DEV_USER_EMAIL`, `DEV_USER_PASSWORD` will be used to generate a test user, but you can register on the site as well
        - `DEV_SEED_USERS` and `DEV_SEED_NOTES` specify how many entries should be seeded into the database
        - `TEST_USER_NAME`, `TEST_USER_EMAIL`, `TEST_USER_PASSWORD` can be used by Dusk for browser testing
        - These all have reasonable values filled in already
3. Generate the application key:
        - `php artisan key:generate`
4. Install the database:
    - If running inside of homestead, you should already know how to create this.
    - If running WITHOUT homestead, you can run `php artisan notes:db:create`
5. Run the migrations and seed the database:
    - `php artisan migrate:fresh --seed`
6. Install dependencies:
    - `composer install`
    - `npm install` or `yarn install`
7. Compile assets:
    - `npm run prod`
8. Add the `APP_URL` to your hosts file (replace this with the url you chose):
    - Linux:
        + Add "APP_URL   192.168.10.10" to the `/etc/hosts` file
    - Windows:
        + Add "APP_URL  192.168.10.10" to the `C:\Windows\System32\drivers\etc\hosts` file
9. **Check out the site:**
    - Run `php artisan serve --port=8282` if you are not running homestead.
    - The default login (*unless you changed .env developer variables*) is:
        + username: `test@test.com`
        + password: `$sh4rpspr1nG$`


## Explanation

- Models: 
    - App\Note - `app/Note.php`
    - App\User - `app/User.php`
    - Both have relationships to each other so that `$user->notes` returns the notes that belong to the user, and `$note->user` will return the user that owns the note.
- Controllers:
    - App\Http\Controllers\NoteController - `app/Http/Controllers/NoteController.php`
    - App\Http\Controllers\HomeController - `app/Http/Controllers/HomeController.php`
    - Both use Authorization middleware and verification for data manipulation
- Middleware
    + Using default authorization middleware from Laravel
- Database:
    + Located at `database/*`
    + Models factories for both models
    + Migrations for both models
    + Database seeders for both models that use the .env configuration to create default users
- Artisan console command:
    + Located at `app/Console/Commands/DatabaseCreateCommand.php`
    + Creates a default database based on .env variables
    + Command line execution is `php artisan notes:db:create`
- VueJS / Javascript:
    + located at `resources/assets/js/*`
        + Dashboard.vue - Vue component for dashboard (/home) page
        + Note.vue - component for notes (/notes) page.
        + These handle the json calls the the controllers, in addition to the entire crud gui
    + There is also a customized webpack mix file at `/webpack.mix.js`
        + This uses library extraction, concatenation, minification, source maps, versioning, and BrowserSync for easier development
        + You can run `npm run watch-poll` to check out the live-reloading BrowserSync
- SASS/CSS:
    + located at `resources/assets/sass/*`
    + These files pull in bootstrap and font assets and include some custom sass as well
- Views/Layouts
    + located at `resources/views/*`
        - Custom app layout (layouts/app.blade.php) uses webpack, shims, laravel stacks, blade templates, and partials
        - Notes page is located at `pages/notes.blade.php`
        - Dashboard page is located at `home.blade.php`
        - Auth layouts have been modified (`auth/*`)
- Routes:
    + located at `routes/*`
        * Only changes are to the `web.php` file, routes have been added as resource controllers
- Tests:
    + There are a couple of tests are located at `tests/*`
    + I started adding some Dusk (browser) tests, but ran out of time.
    + There are tests for login, registering, and notes but I didn't get finished with them. (`tests/Browser/*`)
    + Modified phpunit.xml and phpunit.dusk.xml files for testing
    + You would have to setup [Dusk](https://laravel.com/docs/5.5/dusk) locally to test these properly. Running `phpunit` will cause these tests to fail without the proper setup.
