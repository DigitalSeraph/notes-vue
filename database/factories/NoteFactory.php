<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Note::class, function (Faker $faker) {
    $title = title_case($faker->words(3, true));
    $body = $faker->paragraph;
    $created_at = $faker->dateTimeBetween('-3 years', 'now');
    $updated_at = $faker->dateTimeBetween($created_at, 'now');

    return [
        'title' => $title,
        'body' => $body,
        'user_id' => App\User::all()->random()->id,
        'created_at' => $created_at,
        'updated_at' => $updated_at
    ];
});
