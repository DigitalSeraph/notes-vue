<?php

use Illuminate\Database\Seeder;

class NotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Generate other notes
        $noteSeeds = (int) env('DEV_SEED_NOTES', 20);
        factory(App\Note::class, $noteSeeds)->create();
    }
}
