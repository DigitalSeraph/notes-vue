@extends('layouts.app')

@section('page-header', 'Dashboard')

@section('page-header-subtext', 'Manage your notes')

@section('content')
<dashboard></dashboard>
@endsection


@push('post-scripts')
<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
@endpush
