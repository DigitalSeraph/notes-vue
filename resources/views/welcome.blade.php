@extends('layouts.app')

@section('page-header', 'Welcome to Notes!')

@section('page-header-subtext', '')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Welcome!
                    </div>

                    <div class="panel-body">
                        @guest
                            <p>Please <a href="{{ route('login') }}">login</a> or <a href="{{ route('register') }}">register</a> to start using this application.</p>
                        @else
                            <p>Welcome back <strong>{{ Auth::user()->name }}</strong>!</p>
                            <p>You are already logged in. <a href="{{ route('home') }}">Head on over to your dashboard</a> to manage your notes.</p>
                        @endguest
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
