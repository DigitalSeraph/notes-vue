<?php

namespace Tests;

use App\User;
use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless'
        ]);

        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY, $options
            )
        );
    }

    /**
     * return a User Object that uses the .env variables
     *
     * @return \App\User The user object
     */
    public static function getDotEnvUser()
    {
        $user = new User;
        
        $user->name       = env('TEST_USER_NAME', 'Dusky Tester');
        $user->email      = env('TEST_USER_EMAIL', 'test@dusk.com');
        $user->password   = env('TEST_USER_PASSWORD', 'secret');

        return $user;
    }
}
