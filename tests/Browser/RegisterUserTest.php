<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class RegisterUserTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function testRegisterUser()
    {
        $user = static::getDotEnvUser();

        $this->browse(function (Browser $browser) use ($user) {
            $browser->resize(1920, 1080)
                    ->visit('/register')
                    ->type('name', $user->name)
                    ->type('email', $user->email)
                    ->type('password', $user->password)
                    ->type('password_confirmation', $user->password)
                    ->press('@register-button')
                    ->assertPathIs('/home')
                    ->assertSeeIn('.page-header h1', 'Dashboard');
        });
    }

    /** @test */
    public function testFailOnPasswordToShort()
    {
        $user = static::getDotEnvUser();
        $user->password   = '123';

        $this->browse(function (Browser $browser) use ($user) {
            $browser->resize(1920, 1080)
                    ->visit('/register')
                    ->type('name', $user->name)
                    ->type('email', $user->email)
                    ->type('password', $user->password
)                    ->type('password_confirmation', $user->password)
                    ->press('@register-button')
                    ->assertPathIs('/register')
                    ->assertSeeIn('.help-block', 'The password must be at least 6 characters.');
        });
    }
}
