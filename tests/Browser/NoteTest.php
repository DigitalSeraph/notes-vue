<?php

namespace Tests\Browser;

use App\User;
use App\Note;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class NoteTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testCreateNote()
    {
        $user = factory(User::class)->create([
            'email' => 'taylor@laravel.com'
        ]);

        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                    ->visit('/home');

            $browser->resize(1920, 1080)
                    ->visit('/home')
                    ->assertPathIs('/home')
                    ->assertSeeIn('.page-header h1', 'Dashboard');
        });
    }
}
