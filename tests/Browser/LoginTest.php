<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function testLoginUser()
    {
        $user = factory(User::class)->create([
            'name' => env('TEST_USER_NAME', 'Dusky Tester'),
            'email' => env('TEST_USER_EMAIL', 'test@dusk.com'),
            'password' => env('TEST_USER_PASSWORD', 'secret')
        ]);

        $this->browse(function (Browser $browser) use ($user) {
            $browser->resize(1920, 1080)
                    ->visit('/login')
                    ->type('email', $user->email)
                    ->type('password', $user->password)
                    ->press('@login-button')
                    ->waitForReload()
                    ->assertPathIs('/home')
                    ->assertSeeIn('.page-header h1', 'Dashboard');
        });
    }

    /** @test */
    public function testFailLoginWithBadPassword()
    {
        $user = factory(User::class)->create([
            'email' => 'taylor@laravel.com',
            'password' => bcrypt('secret')
        ]);

        $user = static::getDotEnvUser();
        $user->password = 'somethingrAnd0m';

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit('/login')
                    ->type('@email-field', $user->email)
                    ->type('password', $user->password)
                    ->press('@login-button')
                    ->waitForReload()
                    ->assertPathIs('/login')
                    ->assertSee('These credentials do not match our records.');
        });
    }
}
